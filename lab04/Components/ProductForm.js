import React, { useContext, useState } from "react";
import { ProductsContext } from "../context/products-context";

const ProductForm = () =>
{

    const { dispatch } = useContext(ProductsContext);
    const [productName, setProductName] = useState("");
  
    const handleAddProduct = () => {
      if (productName.trim() !== "") {
        dispatch({ type: "ADD_PRODUCT", name: productName });
        setProductName("");
      }
    };
  
    return(
        <div className="add-product">
        <input
          type="text"
          value={productName}
          onChange={(e) => setProductName(e.target.value)}
          placeholder="Введіть назву товару"
        />
        <button className="add" onClick={handleAddProduct}>
          Додати
        </button>
      </div>
    )
}

export default ProductForm
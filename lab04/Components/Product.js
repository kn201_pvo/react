import React, { useContext, useEffect } from "react";
import { ProductsContext } from "../context/products-context";

const Product = ({ id, name }) => {
  const { dispatch } = useContext(ProductsContext);

  const handleDelete = () => {
    dispatch({ type: "REMOVE_PRODUCT", id: id });
  };

  const handleEdit = () => {
    const newName = prompt("Введіть нову назву товару", name);
    if (newName) {
      dispatch({ type: "EDIT_PRODUCT", payload: { id, newName } });
    }
  };

  useEffect(() => {
    console.log(`Елемент з ID: ${id}, Назва: "${name}" було додано.`);
    return () => {
      console.log(`Елемент з ID: ${id}, Назва: "${name}" було видалено.`);
    };
  }, [id, name]);

  return (
    <div className="product">
      <span>{name}</span>
      <button className="delete" onClick={handleDelete}>
        Видалити
      </button>
      <button className="edit" onClick={handleEdit}>
        Редагувати
      </button>
    </div>
  );
};

export default Product;

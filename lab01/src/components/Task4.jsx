function Task4(props) {
    return (
        <select name="" id="">
            {
                props.cities.map((props) =>(<option value={props.id}>{props.name}</option>))
            }
        </select>
    )
}

export default Task4
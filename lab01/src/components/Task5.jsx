import {useState} from "react";

function Task5(props) {
    const [color, setColor] = useState('red')

    function toggleColor(color) {
        return setColor(color.target.value);
    }

    return (
        <>
        I am a <p style={{color: `${color}`, display: "inline"}}>{color}</p> Product!
            <select onChange={toggleColor}>
                <option value="red">red</option>
                <option value="blue">blue</option>
            </select>
        </>
    )
}

export default Task5
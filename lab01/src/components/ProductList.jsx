import ProductItem from "./ProductItem";
function ProductList(props) {
    return (
        <>
            <h2>List: {props.list.name}</h2>
            <ul>
            { props.list.products.map((product) =>
                <ProductItem item={product}/>
            )}
            </ul>
        </>
    )
}

export default ProductList
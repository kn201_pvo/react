import logo from './logo.svg';
import './App.css';
import './components/Table'
import {useState} from "react";
import Table from "./components/Table";
import Task3 from "./components/Task3";
import Task4 from "./components/Task4";
import Task5 from "./components/Task5";
import ProductList from "./components/ProductList";

function App() {
    const [color, setColor] = useState('red')
    const product1 = {name: "Mouse"}
    const cities = [
        {id: 1, name: "Chicago", image: 'chicago.jpg'},
        {id: 2, name: "Los Angeles", image: 'los-angeles.jpg'},
        {id: 3, name: "New York", image: 'new-york.jpg'},
    ]
    let productList1 = {
        name: 'Products',
        products: [
            {name: 'Product', id: 1, price: 2000, available: 1},
            {name: 'Product', id: 2, price: 3000, available: 0}
        ]
    };
    let productList2 = {
        name: 'Discount products',
        products: [
            {name: 'Product', id: 3, price: 4000, available: 1},
            {name: 'Product', id: 4, price: 5000, available: 0}
        ]
    };
  return (
    <div className="App">
      <Table />
        <div>
            <Task3 product={product1} />
        </div>
        <div>
            <Task4 cities={cities} />
        </div>
        <div>
            <Task5 />
        </div>
        <div>
            <ProductList list={productList1}/>
            <ProductList list={productList2}/>
        </div>
    </div>
  );
}

export default App;

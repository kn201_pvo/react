import logo from './logo.svg';
import './App.css';
import FeedbackForm from './FeedbackForm';
import DeliveryForm from './DeliveryForm';

function App() {
  return (
    <div className="App">
     <DeliveryForm></DeliveryForm>
    </div>
  );
}

export default App;

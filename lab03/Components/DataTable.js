import React, { useState, useEffect } from 'react';
import axios from 'axios';
import ThumbnailLink from './ThumbnailLink';
import ImageViewerModal from './ImageViewerModal';
import Filters from './Filters';
import Pagination from './Pagination';

function DataTable() {
  const [data, setData] = useState([]);
  const [filteredData, setFilteredData] = useState([]);
  const [selectedImage, setSelectedImage] = useState(null);
  const [isViewerOpen, setIsViewerOpen] = useState(false);
  const [currentPage, setCurrentPage] = useState(1);
  const itemsPerPage = 10; // Кількість елементів на сторінці

  useEffect(() => {
    axios.get('https://jsonplaceholder.typicode.com/photos')
      .then((response) => {
        setData(response.data);
      })
      .catch((error) => {
        console.error('Error:', error);
      });
  }, []);

  useEffect(() => {
    // Фільтруємо дані за кількістю слів в полі title (менше або рівно 7)
    const filtered = data.filter((item) => {
      const titleWords = item.title.split(' ');
      return titleWords.length <= 7;
    });

    setFilteredData(filtered);
  }, [data]);

  const handleFilter = (albumFilter, titleFilter) => {
    let filtered = data;

    if (albumFilter) {
      filtered = filtered.filter((item) => item.albumId.toString() === albumFilter);
    }

    if (titleFilter) {
      filtered = filtered.filter((item) =>
        item.title.toLowerCase().includes(titleFilter.toLowerCase())
      );
    }

    setFilteredData(filtered);
  };

  const handleSort = (sortType) => {
    let sortedData = [...filteredData];
    if (sortType === 'id') {
      sortedData.sort((a, b) => a.id - b.id);
    } else if (sortType === 'title') {
      sortedData.sort((a, b) => {
        if (a.title < b.title) {
          return -1;
        }
        if (a.title > b.title) {
          return 1;
        }
        return 0;
      });
    }
    setFilteredData(sortedData);
  };

  // Логіка для пагінації
  const totalItems = filteredData.length;
  const totalPages = Math.ceil(totalItems / itemsPerPage);
  const indexOfLastItem = currentPage * itemsPerPage;
  const indexOfFirstItem = indexOfLastItem - itemsPerPage;
  const currentItems = filteredData.slice(indexOfFirstItem, indexOfLastItem);

  const openImageViewer = (largeUrl) => {
    setSelectedImage(largeUrl);
    setIsViewerOpen(true);
  };

  const closeImageViewer = () => {
    setSelectedImage(null);
    setIsViewerOpen(false);
  };

  const handleNextPage = () => {
    setCurrentPage(currentPage + 1);
  };

  const handlePreviousPage = () => {
    setCurrentPage(currentPage - 1);
  };

  return (
    <div>
      <Filters onFilter={handleFilter} onSort={handleSort} />
      <table>
        <thead>
          <tr>
            <th>ID</th>
            <th>Title</th>
            <th>Thumbnail</th>
          </tr>
        </thead>
        <tbody>
          {currentItems.map((item) => (
            <tr key={item.id}>
              <td>{item.id}</td>
              <td>{item.title}</td>
              <td>
                <ThumbnailLink
                  thumbnailUrl={item.thumbnailUrl}
                  largeUrl={item.url}
                  onThumbnailClick={openImageViewer}
                />
              </td>
            </tr>
          ))}
        </tbody>
      </table>
      <Pagination
        currentPage={currentPage}
        totalPages={totalPages}
        onNext={handleNextPage}
        onPrevious={handlePreviousPage}
      />
      {isViewerOpen && (
        <ImageViewerModal imageUrl={selectedImage} onClose={closeImageViewer} />
      )}
    </div>
  );
}

export default DataTable;

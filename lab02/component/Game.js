import { Button, TextField } from "@mui/material";
import { useState } from "react";

export default function Game() {
    const [isGame, SetGame] = useState(false)
    const [atemps, SetAtemps] = useState(0)
    const [int, SetInt] = useState();
    const [inputInt, SetInputInt] = useState("")

    const [info, SetInfo] = useState([])
    const [res, SetRes] = useState(2);
    const result =
        [
            "Good Job",
            "Game Over!",
            ""
        ]
    const StartGame = () => {
        SetGame(true)
        SetInt(Math.round(Math.random() * 100))
        SetAtemps(0)
        SetInfo([])
        SetRes(2)
    }

    const CheckAnswer = () => {
        if (inputInt != "") {
            SetAtemps(prev => prev + 1)
            console.log(int);
            if (int === inputInt) {
                SetRes(0)
                SetGame(false)
            }
            if (int > inputInt) {
                SetInfo(prev => [...prev, { id: info.length, info: "> " + inputInt }])
            }
            if (int < inputInt) {
                SetInfo(prev => [...prev, { id: info.length, info: "< " + inputInt }])
            }
            if (atemps >= 4) {
                SetRes(1)
                SetGame(false)
            }
            SetInputInt('');
        }
    }
    return (
        <>
            <Button variant="contained" disabled={isGame} onClick={StartGame}>Start Game</Button>
            <TextField disabled={!isGame} type="text" onChange={(e) => SetInputInt(+e.target.value)} value={inputInt}></TextField>
            <Button variant="contained" disabled={!isGame} onClick={CheckAnswer}> Check</Button>
            <h3>Information</h3>
            {
                info.length != 0 ?
                    info.map(inf =>
                        <p key={inf.id}>
                            {inf.info}
                        </p>
                    ) : <></>
            }
            <h3>Atemps: {atemps}</h3>
            <h2>Result: {result[res]}</h2>
        </>
    );
}

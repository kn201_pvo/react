import { useState } from "react";
import Input from "./Input";
import Product from "./Product";

export default function ProductPage() {
    const [products, setProducts] = useState([
        { id: 0, name: "Cousin" },
        { id: 1, name: "Slave" }
    ]);

    const ChangeState = (name) => {
        setProducts((prevProducts) => [
            ...prevProducts,
            { id: prevProducts.length, name: name }
        ]);
    };

    const DeleteProduct = (id) => {
        setProducts((Products) =>
            Products.filter((element) => {
                return element.id !== id;
            }))
    }

    return (
        <>
            <Input ChangeState={ChangeState}></Input>

            {
                products.map((prod) =>
                    <Product key={prod.id} id={prod.id} name={prod.name} resetFunc={DeleteProduct} />
                )
            }

        </>
    );
}